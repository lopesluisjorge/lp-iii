/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Animal;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorAnimal;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.AnimalNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JPanelBotaoFecharTela;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JPanelPainelTitulo;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaBuscaAnimal extends JFrame {

    private JTextField campoIndice;

    public TelaBuscaAnimal() {
        super("Tela de Busca de Animal");
        this.setUp();
    }

    private void setUp() {
        JPanel painelTitulo = new JPanelPainelTitulo("Digite o ídice do Animal");

        this.campoIndice = new JTextField("", 10);
        JButton botaoSubmissao = new JButton("Buscar");

        JPanel painel = new JPanel();

        painel.add(this.campoIndice);
        painel.add(botaoSubmissao);

        botaoSubmissao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Animal animal = ControladorAnimal.recuperarAnimal(Integer.parseInt(campoIndice.getText()));
                    new TelaDetalhesAnimal(animal);
                } catch (AnimalNaoEncontradoException err) {
                    new TelaInformacaoNaoEncontrada("Animal não encontrado");
                }

                dispose();
            }
        });

        JPanelBotaoFecharTela painelVoltar = new JPanelBotaoFecharTela(this);

        this.add(BorderLayout.CENTER, painel);
        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.SOUTH, painelVoltar);

        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
