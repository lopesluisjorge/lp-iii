/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorAutenticacao;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorInicial;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Usuario;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaRegistroUsuario extends JFrame {

    private JTextField campoNickname;
    private JPasswordField campoSenha;

    public TelaRegistroUsuario() {
        super("Registro de Usuário");
        this.setUp();
    }

    private void setUp() {
        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(3, 1);
        painelForm.setLayout(layoutPanel);

        JPanel painel0 = new JPanel();

        FlowLayout layoutLabels = new FlowLayout();
        layoutLabels.setAlignment(FlowLayout.LEFT);

        this.campoNickname = new JTextField("", 20);
        painel0.setLayout(layoutLabels);
        painel0.add(new JLabel("Login"));
        painel0.add(this.campoNickname);

        painelForm.add(painel0);

        JPanel painel1 = new JPanel();

        this.campoSenha = new JPasswordField("", 20);
        painel1.setLayout(layoutLabels);
        painel1.add(new JLabel("Senha"));
        painel1.add(this.campoSenha);

        painelForm.add(painel1);

        JPanel painelTopo = new JPanel();

        JLabel titulo = new JLabel("Registro de Usuário");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        painelTopo.add(titulo);

        JPanel painelBotoes = new JPanel();

        JButton botaoregistro = new JButton("Registrar");
        JButton botaoSair = new JButton("Sair");

        botaoregistro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nickname = campoNickname.getText();
                String senha = campoSenha.getText();

                Usuario usuario = new Usuario(nickname, senha);

                if (ControladorAutenticacao.registrar(usuario)) {
                    if (ControladorAutenticacao.isLogado()) {
                        ControladorInicial.mostrarTelaInicial();
                    } else {
                        ControladorAutenticacao.mostrarTelaLogin();
                    }

                    dispose();
                }

            }
        });
        
        botaoSair.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (ControladorAutenticacao.isLogado()) {
                    new TelaInicial();
                } else {
                    new TelaLogin();
                }
                
                dispose();
            }
        });

        painelBotoes.add(botaoregistro);
        painelBotoes.add(botaoSair);

        this.add(BorderLayout.NORTH, painelTopo);
        this.add(BorderLayout.CENTER, painelForm);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
