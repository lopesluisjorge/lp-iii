/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.UsuarioOuSenhaInvalidaException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Usuario;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.repository.RepositorioUsuarios;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.TelaLogin;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.TelaRegistroUsuario;

/**
 *
 * @author jorge
 */
public class ControladorAutenticacao {
    
    private static RepositorioUsuarios usuarios = new RepositorioUsuarios();
    private static boolean logado = false;
    
    public static void mostrarTelaLogin() {
        new TelaLogin();
    }
    
    public static void mostrarTelaRegistroUsuario() {
        new TelaRegistroUsuario();
    }
    
    public static boolean registrar(Usuario usuario) {
        usuarios.adicionarUsuario(usuario);
        return true;
    }
    
    public static boolean login(String nickname, String senha) {        
        Usuario usuario = usuarios.recuperarUsuario(nickname);
        
        if (!usuario.getSenha().equals(senha)) {
            throw new UsuarioOuSenhaInvalidaException();
        }
        
        logado = true;
        return true;
    }
    
    public static boolean logout() {
        logado = false;
        return true;
    }
    
    public static boolean isLogado() {
        return logado;
    }
    
}
