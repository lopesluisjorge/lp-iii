/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.repository;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.UsuarioNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Usuario;
import java.util.HashMap;

/**
 *
 * @author jorge
 */
public class RepositorioUsuarios {

    private HashMap<String, Usuario> usuarios;

    public RepositorioUsuarios() {
        this.usuarios = new HashMap<>();
    }

    public void adicionarUsuario(Usuario usuario) {
        if (this.usuarios.containsKey(usuario.getNickname())) {
            throw  new RuntimeException("Usuário já existe");
        }
        
        this.usuarios.put(usuario.getNickname(), usuario);
    }

    public Usuario recuperarUsuario(String nickname) {
        if (!this.usuarios.containsKey(nickname)) {
            throw new UsuarioNaoEncontradoException();
        }
        
        Usuario usuario = this.usuarios.get(nickname);
        
        return usuario;
    }

}
