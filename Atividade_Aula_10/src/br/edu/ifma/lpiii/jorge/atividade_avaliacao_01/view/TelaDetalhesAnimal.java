/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Animal;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JPanelBotaoFecharTela;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JPanelPainelTitulo;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.GregorianCalendar;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaDetalhesAnimal extends JFrame {

    private Animal animal;

    public TelaDetalhesAnimal(Animal animal) {
        super("Detalhes do animal");
        this.animal = animal;
        this.setUp();
    }

    private void setUp() {
        JPanelPainelTitulo painelTitulo = new JPanelPainelTitulo("Detalhes do Animal");

        JLabel campoNome = new JLabel(this.animal.getNome());
        JLabel campoEspecie = new JLabel(this.animal.getEspecie());
        JLabel campoRaca = new JLabel(this.animal.getRaca());
        JLabel campoDataNascimento = new JLabel(""
                + this.animal.getDataNascimento().get(GregorianCalendar.DAY_OF_MONTH)
                + "/" + this.animal.getDataNascimento().get(GregorianCalendar.MONTH)
                + "/" + this.animal.getDataNascimento().get(GregorianCalendar.YEAR));
        JLabel campoIdade = new JLabel("" + (new GregorianCalendar().get(GregorianCalendar.YEAR) - this.animal.getDataNascimento().get(GregorianCalendar.YEAR)));
        JLabel campoPeso = new JLabel("" + this.animal.getPeso());

        JPanel painelRotulos = new JPanel();
        GridLayout layoutPanel = new GridLayout(7, 2);
        painelRotulos.setLayout(layoutPanel);

        painelRotulos.add(new JLabel("Nome"));
        painelRotulos.add(campoNome);
        painelRotulos.add(new JLabel("Espécie"));
        painelRotulos.add(campoEspecie);
        painelRotulos.add(new JLabel("Raça"));
        painelRotulos.add(campoRaca);
        painelRotulos.add(new JLabel("Data de Nascimento"));
        painelRotulos.add(campoDataNascimento);
        painelRotulos.add(new JLabel("Idade"));
        painelRotulos.add(campoIdade);
        painelRotulos.add(new JLabel("Peso"));
        painelRotulos.add(campoPeso);

        JPanelBotaoFecharTela painelFecharTela = new JPanelBotaoFecharTela(this);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painelRotulos);
        this.add(BorderLayout.SOUTH, painelFecharTela);

        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
