/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components;

import java.awt.Font;
import javax.swing.JLabel;

/**
 *
 * @author jorge
 */
public class JLabelTitulo extends JLabel {

    public JLabelTitulo(String titulo) {
        super(titulo);
        this.setFont(new Font("Tahoma", Font.BOLD, 16));
    }

    
}
