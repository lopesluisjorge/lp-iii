/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Animal;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorAnimal;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JButtonSair;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JPanelPainelTitulo;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaCadastroAnimal extends JFrame {

    private JTextField campoNome;

    private JTextField campoEspecie;

    private JTextField campoRaca;

    private JTextField campoDiaNascimento;

    private JTextField campoMesNascimento;

    private JTextField campoAnoNascimento;

    private JTextField campoPeso;

    public TelaCadastroAnimal() {
        super("Cadastro de Animal");
        this.setUp();
    }

    private void setUp() {
        JPanelPainelTitulo painelTitulo = new JPanelPainelTitulo("Cadastro de Animal");

        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(5, 1);
        painelForm.setLayout(layoutPanel);

        FlowLayout layoutLabels = new FlowLayout();
        layoutLabels.setAlignment(FlowLayout.LEFT);

        JPanel painel0 = new JPanel();

        this.campoNome = new JTextField("", 20);
        painel0.setLayout(layoutLabels);
        painel0.add(new JLabel("Nome"));
        painel0.add(this.campoNome);

        JPanel painel1 = new JPanel();

        this.campoEspecie = new JTextField("", 20);
        painel1.setLayout(layoutLabels);
        painel1.add(new JLabel("Espécie"));
        painel1.add(this.campoEspecie);

        JPanel painel2 = new JPanel();

        this.campoRaca = new JTextField("", 20);
        painel2.setLayout(layoutLabels);
        painel2.add(new JLabel("Raça"));
        painel2.add(this.campoRaca);

        JPanel painel3 = new JPanel();
        painel3.setLayout(new GridLayout(1, 6));

        painel3.add(new JLabel("Data do Nascimento"));

        this.campoDiaNascimento = new JTextField("", 4);
        painel3.setLayout(layoutLabels);
        painel3.add(this.campoDiaNascimento);
        painel3.add(new JLabel("/"));
        this.campoMesNascimento = new JTextField("", 4);
        painel3.setLayout(layoutLabels);
        painel3.add(this.campoMesNascimento);
        painel3.add(new JLabel("/"));
        this.campoAnoNascimento = new JTextField("", 8);
        painel3.setLayout(layoutLabels);
        painel3.add(this.campoAnoNascimento);

        JPanel painel4 = new JPanel();

        this.campoPeso = new JTextField("", 20);
        painel4.setLayout(layoutLabels);
        painel4.add(new JLabel("Peso"));
        painel4.add(this.campoPeso);

        painelForm.add(painel0);
        painelForm.add(painel1);
        painelForm.add(painel2);
        painelForm.add(painel3);
        painelForm.add(painel4);

        JPanel painelBotoes = new JPanel();

        JButton botaoNovo = new JButton("Salvar");
        JButtonSair botaoCancelar = new JButtonSair("Cancelar", this);

        botaoNovo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GregorianCalendar dataNascimento = new GregorianCalendar(Integer.parseInt(campoAnoNascimento.getText()), Integer.parseInt(campoMesNascimento.getText()), Integer.parseInt(campoDiaNascimento.getText()));

                Animal animal = new Animal(campoEspecie.getText(), campoRaca.getText(), dataNascimento);
                animal.setNome(campoNome.getText());
                animal.setPeso(Double.parseDouble(campoPeso.getText()));

                ControladorAnimal.adicionarAnimal(animal);

                dispose();
            }
        });

        painelBotoes.add(botaoNovo);
        painelBotoes.add(botaoCancelar);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painelForm);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
