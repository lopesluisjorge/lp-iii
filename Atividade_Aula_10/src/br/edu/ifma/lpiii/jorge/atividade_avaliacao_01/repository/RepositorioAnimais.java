/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.repository;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.AnimalNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Animal;
import java.util.ArrayList;

/**
 *
 * @author jorge
 */
public class RepositorioAnimais {

    private ArrayList<Animal> animais;

    public RepositorioAnimais() {
        this.animais = new ArrayList<>();
    }

    public void adicionarAnimal(Animal animal) {
        this.animais.add(animal);
    }

    public Animal recuperarAnimal(int indice) {
        if (indice > animais.size() - 1) {
            throw new AnimalNaoEncontradoException();
        }
        
        Animal animal = this.animais.get(indice);
        
        return animal;
    }

}
