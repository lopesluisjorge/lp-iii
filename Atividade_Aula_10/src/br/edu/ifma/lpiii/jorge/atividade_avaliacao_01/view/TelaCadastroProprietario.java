/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorProprietario;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Proprietario;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JButtonSair;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaCadastroProprietario extends JFrame {

    private JTextField campoNome;

    private JTextField campoCpf;

    private JTextField campoEndereco;

    private JTextField campoTelefone;

    public TelaCadastroProprietario() {
        super("Cadastro de Proprietário de Animal");
        this.setUp();
    }

    private void setUp() {
        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(4, 1);
        painelForm.setLayout(layoutPanel);

        JPanel painel0 = new JPanel();

        FlowLayout layoutLabels = new FlowLayout();
        layoutLabels.setAlignment(FlowLayout.LEFT);

        this.campoNome = new JTextField("", 20);
        painel0.setLayout(layoutLabels);
        painel0.add(new JLabel("Nome"));
        painel0.add(this.campoNome);

        painelForm.add(painel0);

        JPanel painel1 = new JPanel();

        this.campoCpf = new JTextField("", 20);
        painel1.setLayout(layoutLabels);
        painel1.add(new JLabel("CPF"));
        painel1.add(this.campoCpf);

        painelForm.add(painel1);

        JPanel painel2 = new JPanel();

        this.campoEndereco = new JTextField("", 20);
        painel2.setLayout(layoutLabels);
        painel2.add(new JLabel("Endereço"));
        painel2.add(this.campoEndereco);

        painelForm.add(painel2);

        JPanel painel4 = new JPanel();

        this.campoTelefone = new JTextField("", 20);
        painel4.setLayout(layoutLabels);
        painel4.add(new JLabel("Telefone"));
        painel4.add(this.campoTelefone);

        painelForm.add(painel4);

        JLabel titulo = new JLabel("Cadastro do Proprietario");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        JPanel painelBotoes = new JPanel();

        JButton botaoNovo = new JButton("Salvar");
        JButtonSair botaoCancelar = new JButtonSair("Cancelar", this);

        botaoNovo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nome = campoNome.getText();
                String cpf = "" + Long.parseUnsignedLong(campoCpf.getText());
                String endereco = campoEndereco.getText();
                String telefone = "" + Long.parseUnsignedLong(campoTelefone.getText());

                Proprietario proprietario = new Proprietario(nome, cpf, endereco, telefone);

                ControladorProprietario.adicionarProprietario(proprietario);

                dispose();
            }
        });

        painelBotoes.add(botaoNovo);
        painelBotoes.add(botaoCancelar);

        this.add(BorderLayout.NORTH, titulo);
        this.add(BorderLayout.CENTER, painelForm);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
