/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view;

import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.controller.ControladorProprietario;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.AnimalNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.exception.ProprietarioNaoEncontradoException;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.model.Proprietario;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JButtonSair;
import br.edu.ifma.lpiii.jorge.atividade_avaliacao_01.view.components.JPanelPainelTitulo;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaAdicionarAnimaisAProprietario extends JFrame {

    private JTextField campoIdProprietario;
    private JTextField campoIdAnimal;

    public TelaAdicionarAnimaisAProprietario() {
        super("Adicionar Animal a Proprietario");
        this.setUp();

    }

    private void setUp() {

        JPanelPainelTitulo painelTitulo = new JPanelPainelTitulo("Adicionar Animal a proprietário");

        JPanel painelFormulario = new JPanel();

        GridLayout layoutPanel = new GridLayout(3, 1);

        painelFormulario.setLayout(layoutPanel);

        JPanel painel0 = new JPanel();

        FlowLayout layoutLabels = new FlowLayout();
        layoutLabels.setAlignment(FlowLayout.LEFT);

        this.campoIdProprietario = new JTextField("", 20);
        painel0.setLayout(layoutLabels);
        painel0.add(new JLabel("ID Proprietário"));
        painel0.add(this.campoIdProprietario);

        painelFormulario.add(painel0);

        JPanel painel1 = new JPanel();

        this.campoIdAnimal = new JTextField("", 20);
        painel1.setLayout(layoutLabels);
        painel1.add(new JLabel("Id Animal"));
        painel1.add(this.campoIdAnimal);

        painelFormulario.add(painel1);

        JButton botaoSalvar = new JButton("Salvar");
        JButtonSair botaoCancelar = new JButtonSair("Cancelar", this);

        botaoSalvar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int idProprietario = Integer.parseInt(campoIdProprietario.getText());
                    int idAnimal = Integer.parseInt(campoIdAnimal.getText());

                    Proprietario p = ControladorProprietario.adicionarAnimalAProprietario(idProprietario, idAnimal);

                    System.out.println(p);
                } catch (ProprietarioNaoEncontradoException | AnimalNaoEncontradoException err) {
                    new TelaInformacaoNaoEncontrada("Proprietário ou animal não encontrado");
                }

                dispose();
            }
        });

        JPanel painelBotoes = new JPanel();

        painelBotoes.add(botaoSalvar);
        painelBotoes.add(botaoCancelar);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painelFormulario);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
