/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_09.model;

import br.edu.ifma.lpiii.jorge.atividade_aula_09.exception.AnimalNaoEncontradoException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author jorge
 */
public class Proprietario {

    private String nome;

    private String cpf;

    private String endereco;

    private String telefone;

    private ArrayList<Animal> animais;

    public Proprietario(String nome, String cpf, String endereco, String telefone) {
        this.nome = nome;
        this.cpf = cpf;
        this.endereco = endereco;
        this.telefone = telefone;
        this.animais = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void adicionarAnimal(Animal animal) {
        if (animal == null) {
            throw new RuntimeException();
        }

        this.animais.add(animal);
    }

    public Animal recuperarAnimal(int indice) {
        if (indice > this.animais.size() - 1) {
            throw new AnimalNaoEncontradoException();
        }

        return this.animais.get(indice);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Nome: " + this.nome + "\n");
        builder.append("CPF: " + this.cpf + "\n");
        builder.append("Endereço: " + this.endereco + "\n");
        builder.append("Telefone: " + this.telefone + "\n");
        builder.append("Animais: ");
        
        Iterator<Animal> iterator = animais.iterator();
        
        while (iterator.hasNext()) {
            builder.append(iterator.next());
        }
        
        for (Animal animal : animais) {
            
        }

        return builder.toString();
    }

}
