/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_09.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaInformacaoNaoEncontrada extends JFrame {
    
    public TelaInformacaoNaoEncontrada(String titulo) {
        super(titulo);
        this.setUp(titulo);
    }
    
    public void setUp(String titulo) {
        JPanel painel = new JPanel();
        
        JLabel lblNaoEncontrado = new JLabel(titulo);

        lblNaoEncontrado.setFont(new Font("Tahoma", Font.ITALIC | Font.BOLD, 20));
        lblNaoEncontrado.setForeground(Color.red);
        
        painel.add(lblNaoEncontrado);
        
        JPanel painelBotaoVoltar = new JPanel();
        JButton botaoVoltar = new JButton("Voltar");
        
        botaoVoltar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        
        painelBotaoVoltar.add(botaoVoltar);
        
        this.add(BorderLayout.CENTER, painel);
        this.add(BorderLayout.SOUTH, painelBotaoVoltar);
        
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }
    
}
