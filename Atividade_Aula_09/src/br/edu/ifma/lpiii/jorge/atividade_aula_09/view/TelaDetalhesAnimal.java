/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_09.view;

import br.edu.ifma.lpiii.jorge.atividade_aula_09.model.Animal;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.GregorianCalendar;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaDetalhesAnimal extends JFrame {

    private Animal animal;
    private JLabel campoNome;
    private JLabel campoEspecie;
    private JLabel campoRaca;
    private JLabel campoDataNascimento;
    private JLabel campoIdade;
    private JLabel campoPeso;

    public TelaDetalhesAnimal(Animal animal) {
        super("Detalhes do animal");
        this.animal = animal;
        this.setUp();
    }

    private void setUp() {
        this.campoNome = new JLabel(this.animal.getNome());
        this.campoEspecie = new JLabel(this.animal.getEspecie());
        this.campoRaca = new JLabel(this.animal.getRaca());
        this.campoDataNascimento = new JLabel(""
                + this.animal.getDataNascimento().get(GregorianCalendar.DAY_OF_MONTH)
                + "/" + this.animal.getDataNascimento().get(GregorianCalendar.MONTH)
                + "/" + this.animal.getDataNascimento().get(GregorianCalendar.YEAR));
        this.campoIdade = new JLabel("" + (new GregorianCalendar().get(GregorianCalendar.YEAR) - this.animal.getDataNascimento().get(GregorianCalendar.YEAR)));
        this.campoPeso = new JLabel("" + this.animal.getPeso());

        JPanel painelRotulos = new JPanel();
        GridLayout layoutPanel = new GridLayout(6, 2);
        painelRotulos.setLayout(layoutPanel);

        painelRotulos.add(new JLabel("Nome"));
        painelRotulos.add(this.campoNome);
        painelRotulos.add(new JLabel("Espécie"));
        painelRotulos.add(this.campoEspecie);
        painelRotulos.add(new JLabel("Raça"));
        painelRotulos.add(this.campoRaca);
        painelRotulos.add(new JLabel("Data de Nascimento"));
        painelRotulos.add(this.campoDataNascimento);
        painelRotulos.add(new JLabel("Idade"));
        painelRotulos.add(this.campoIdade);
        painelRotulos.add(new JLabel("Peso"));
        painelRotulos.add(this.campoPeso);

        JLabel titulo = new JLabel("Detalhes do Animal");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        this.add(BorderLayout.NORTH, titulo);
        this.add(BorderLayout.CENTER, painelRotulos);

        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
