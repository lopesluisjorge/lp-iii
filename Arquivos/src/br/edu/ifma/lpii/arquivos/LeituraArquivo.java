/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpii.arquivos;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author jorge
 */
public class LeituraArquivo {
    
    public static void main(String[] args) {
        
        try {
            try (Scanner arquivo = new Scanner(new File("arquivo.txt"))) {
                while (arquivo.hasNext()) {
                    String leitura = arquivo.nextLine();
                    System.out.println(leitura);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
}
