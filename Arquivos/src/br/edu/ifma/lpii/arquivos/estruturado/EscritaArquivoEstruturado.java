/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpii.arquivos.estruturado;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorge
 */
public class EscritaArquivoEstruturado {

    public static void main(String[] args) {

        FileWriter arquivo = null;

        Objeto obj = new Objeto(0, "String");
        
        
        try {
            arquivo = new FileWriter(new File("objects"), true);
            
            BufferedWriter buffer = new BufferedWriter(arquivo);
            
            buffer.write(obj.getInteger() + ";");
            buffer.write(obj.getStr());
            buffer.newLine();
            
            buffer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
            
            
        

    }
}
