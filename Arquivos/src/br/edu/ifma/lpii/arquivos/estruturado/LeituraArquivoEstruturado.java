/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpii.arquivos.estruturado;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author jorge
 */
public class LeituraArquivoEstruturado {

    public static void main(String[] args) {

        ArrayList<Objeto> objetos = new ArrayList<>();

        Scanner arquivo = null;

        try {
            arquivo = new Scanner(new File("objects"));

            while (arquivo.hasNext()) {
                String leitura = arquivo.nextLine();

                StringTokenizer token = new StringTokenizer(leitura, ";");
                int integer = Integer.parseInt(token.nextToken());
                String str = token.nextToken();

                Objeto obj = new Objeto(integer, str);
                objetos.add(obj);
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (arquivo != null) {
                arquivo.close();
            }
        }

        for (Objeto objeto : objetos) {
            System.out.println(objeto);
        }
    }
}
