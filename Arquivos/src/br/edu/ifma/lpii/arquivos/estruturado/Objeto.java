/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpii.arquivos.estruturado;

/**
 *
 * @author jorge
 */
public class Objeto {

    private int integer;
    private String str;

    public Objeto(int integer, String str) {
        this.integer = integer;
        this.str = str;
    }

    public int getInteger() {
        return integer;
    }

    public String getStr() {
        return str;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Integer: " + this.integer);
        builder.append(" ");
        builder.append("Str: " + this.str);

        return builder.toString();
    }

}
