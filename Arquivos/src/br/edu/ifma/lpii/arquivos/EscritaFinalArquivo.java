/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpii.arquivos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author jorge
 */
public class EscritaFinalArquivo {
    
    public static void main(String[] args) {
       
        try {
            FileWriter arquivo;
            arquivo = new FileWriter(new File("arquivo.txt"), true);
            
            BufferedWriter buffer = new BufferedWriter(arquivo);

            buffer.write("Linha 1");
            buffer.newLine();
            buffer.write("Linha 2");
            buffer.newLine();
            buffer.write("Linha 3");
            buffer.newLine();
            
            buffer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        
    }
    
}
