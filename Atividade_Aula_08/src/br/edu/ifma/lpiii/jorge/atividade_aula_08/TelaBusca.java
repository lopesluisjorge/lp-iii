/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_08;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaBusca extends JFrame {

    private JTextField campoIndice;
    private JButton botaoSubmissao;
    
    public TelaBusca() {
        super("");
        this.setUp();
    }
    
    private void setUp() {
        this.campoIndice = new JTextField("", 10);
        this.botaoSubmissao = new JButton("Buscar");
        
        JLabel titulo = new JLabel("Digite o ídice do Animal");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        this.add(BorderLayout.NORTH, titulo);
        
        JPanel painel = new JPanel();
        
        painel.add(this.campoIndice);
        painel.add(this.botaoSubmissao);
        
        this.add(BorderLayout.CENTER, painel);
        
        this.botaoSubmissao.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Animal animal = Controlador.recuperarAnimal(Integer.parseInt(campoIndice.getText()));
                
                if (animal != null) {
                    System.out.println("Animal não encontrado");
                    dispose();
                }
                
                new TelaDetalhesAnimal(animal);
                dispose();
            }
        });
        
        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }
    
}
