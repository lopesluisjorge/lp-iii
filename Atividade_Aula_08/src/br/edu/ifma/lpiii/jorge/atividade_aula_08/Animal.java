/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_08;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

/**
 *
 * @author jorge
 */
public class Animal {
    
    private static int ultimoInserido = -1;
    
    private String nome;
    
    private String especie;
    
    private String raca;
    
    private GregorianCalendar nascimento;

    private double peso;
    
    public Animal(String especie, String raca, GregorianCalendar nascimento) {
        this.especie = especie;
        this.raca = raca;
        this.nascimento = nascimento;
        
        ultimoInserido++;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public GregorianCalendar getDataNascimento() {
        return nascimento;
    }

    public void setDataNascimento(GregorianCalendar nascimento) {
        this.nascimento = nascimento;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(nome + "; ");
        builder.append(especie + "; ");
        builder.append(raca + "; ");
        builder.append(nascimento);
        
        return builder.toString();
    }

    
    
    
    
    
}
