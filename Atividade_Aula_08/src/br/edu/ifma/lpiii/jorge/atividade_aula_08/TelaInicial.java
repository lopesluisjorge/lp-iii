/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_08;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaInicial extends JFrame {

    private JButton btnCadastro;
    private JButton btnConsulta;
    
    public TelaInicial() {
        super("Tela Inicial");
        this.setUp();
    }
    
    private void setUp() {
        this.setLayout(new FlowLayout());
        
        JPanel painelTela = new JPanel();
        GridLayout layout = new GridLayout(2, 1);
        painelTela.setLayout(layout);
        
        this.btnCadastro = new JButton("Cadastro de animal");
        this.btnConsulta = new JButton("Consulta de animal");
        
        this.btnCadastro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Controlador.telaCadastroAnimal();
            }
        });
        
        this.btnConsulta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Controlador.telaConsultaAnimal();
            }
        });
        
        painelTela.add(this.btnCadastro);
        painelTela.add(this.btnConsulta);
        
        JLabel titulo = new JLabel("Sistema de Cadastro de Animais");
        titulo.setFont(new Font("Tahoma", Font.BOLD, 16));

        this.add(titulo);
        
        this.add(painelTela);
        
        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    
}
