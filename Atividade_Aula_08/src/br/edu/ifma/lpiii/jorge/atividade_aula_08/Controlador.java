/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.jorge.atividade_aula_08;

/**
 *
 * @author jorge
 */
public class Controlador {
    
    private static RepositorioAnimais repositorio = new RepositorioAnimais();

   
    
    public static void telaInicial() {
        new TelaInicial();
    }
    
    public static void telaCadastroAnimal() {
        new TelaCadastroAnimal();
    }
    
    public static void telaConsultaAnimal() {
        new TelaBusca();
    }
    
    public static void telaDetalhesAnimal(int indice) {
        Animal animal = Controlador.recuperarAnimal(indice);
        
        new TelaDetalhesAnimal(animal);
    }
    
    public static void adicionarAnimal(Animal animal) {
        repositorio.adicionarAnimal(animal);
    }
    
    
    
    public static Animal recuperarAnimal(int indice) {
        return repositorio.recuperarAnimal(indice);
    }

    
    
}
