/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.unidade08;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author jorge
 */
final public class TelaInicial extends JFrame {

    private JButton semGerenciador;
    private JButton gerencadorBorderLayout;
    private JButton gerenciadorFlowfayout;
    private JButton gerenciadorGridLayout;
    private JButton gerenciadorGridBagLayout;

    public TelaInicial() {
        super("Tela Inicial");
        this.setUp();
    }

    public void setUp() {
        this.semGerenciador = new JButton("Sem Gerenciador");
        this.gerencadorBorderLayout = new JButton("BorderLayout");
        this.gerenciadorFlowfayout = new JButton("FlowLayout");
        this.gerenciadorGridLayout = new JButton("GridLayout");
        this.gerenciadorGridBagLayout = new JButton("GridBagLayout");

        this.semGerenciador.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaSemGerenciador();
            }
        });

        this.gerencadorBorderLayout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaBorderLayout();
            }
        });

        this.gerenciadorFlowfayout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaFlowLayout();
            }
        });

        this.gerenciadorGridLayout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaGridLayout();
            }
        });

        this.gerenciadorGridBagLayout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TelaGridBagLayout gui = new TelaGridBagLayout();
            }
        });

        this.setLayout(new FlowLayout());

        this.add(this.semGerenciador);
        this.add(this.gerencadorBorderLayout);
        this.add(this.gerenciadorFlowfayout);
        this.add(this.gerenciadorGridLayout);
        this.add(this.gerenciadorGridBagLayout);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }

}
