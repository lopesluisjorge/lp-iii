/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.unidade08;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

/**
 *
 * @author jorge
 */
public class TelaGridBagLayout extends JFrame {

    public TelaGridBagLayout() {
        super("Tela GridBagLayout");
        this.setUp();
    }

    private void setUp() {
        GridBagLayout layout = new GridBagLayout();
        GridBagConstraints constraints = new GridBagConstraints();

        this.setLayout(layout);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1;

        JButton botao1 = new JButton("Botão 1");
        this.adicionarComponente(botao1, layout, constraints);

        JButton botao2 = new JButton("Botão 2");
        this.adicionarComponente(botao2, layout, constraints);

        JButton botao3 = new JButton("Botão 3");
        this.adicionarComponente(botao3, layout, constraints);
        
        constraints.gridwidth = GridBagConstraints.REMAINDER;

        JButton botao4 = new JButton("Botao 4");
        this.adicionarComponente(botao4, layout, constraints);

        constraints.weightx = 1;

        JButton botao5 = new JButton("Botao 5");
        this.adicionarComponente(botao5, layout, constraints);

        String opcoes[] = {
            "Opcão 1", "Opcão 2", "Opcão 3"
        };

        JComboBox combo = new JComboBox(opcoes);
        this.adicionarComponente(combo, layout, constraints);

        constraints.gridwidth = GridBagConstraints.RELATIVE;

        JButton botao6 = new JButton("Botao 6");
        this.adicionarComponente(botao6, layout, constraints);

        constraints.gridwidth = GridBagConstraints.REMAINDER;

        JButton botao7 = new JButton("Botao 7");
        this.adicionarComponente(botao7, layout, constraints);

        constraints.gridwidth = 1;
        constraints.gridheight = 2;
        constraints.weighty = 1;

        JButton botao8 = new JButton("Botao 8");
        this.adicionarComponente(botao8, layout, constraints);

        constraints.weighty = 0;
        constraints.gridwidth = GridBagConstraints.REMAINDER;
        constraints.gridheight = 1;

        JButton botao9 = new JButton("Botao 9");
        this.adicionarComponente(botao9, layout, constraints);

        JButton botao10 = new JButton("Botao 10");
        this.adicionarComponente(botao10, layout, constraints);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.pack();
        this.setSize(new Dimension(480, 360));
        this.setLocation((screenSize.width) / 2, (screenSize.height) / 2);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    private void adicionarComponente(Component componente, GridBagLayout layout, GridBagConstraints constraints) {
        layout.setConstraints(componente, constraints);
        this.add(componente);
    }

}
