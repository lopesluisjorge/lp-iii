/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.unidade08;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author jorge
 */
public class TelaGridLayout extends JFrame {

    private int troca;
    private ArrayList<GridLayout> layouts;

    public TelaGridLayout() {
        super("Tela GridLayout");
        this.troca = 1;
        this.setUp();
    }

    private void setUp() {
        this.layouts = new ArrayList<>();

        this.layouts.add(new GridLayout());
        this.layouts.add(new GridLayout(1, 6, 5, 5));
        this.layouts.add(new GridLayout(3, 2));
        this.layouts.add(new GridLayout(3, 2, 5, 5));

        this.setLayout(layouts.get(0));

        for (int i = 0; i < 6; ++i) {
            JButton b = new JButton("Botão " + i);
            b.addActionListener(new AcaoPerformda());
            this.add(b);
        }

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }

    class AcaoPerformda implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            troca = (troca == 1)
                    ? 2
                    : (troca == 2)
                            ? 3
                            : (troca == 3) ? 4 : 1;
            setLayout(layouts.get(troca - 1));
            validate();
        }

    }

}
