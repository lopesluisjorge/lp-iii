/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.unidade06.teclado;

import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author jorge
 */
public final class GuiEventosTeclado extends JFrame {

    private JLabel label;

    public GuiEventosTeclado() throws HeadlessException {
        super("Gui Eventos Teclado");
        this.setUp();
    }

    public void setUp() {
        this.label = new JLabel("Evento");
        this.addKeyListener(new EventosTeclado());
        
        this.add(this.label);

        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    public void showEvent(KeyEvent evt) {
        System.out.println(evt);
    }

    class EventosTeclado implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            label.setText("Tecla pressionada e liberda");
            showEvent(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            label.setText("Tecla pressionada");
            showEvent(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            label.setText("Tecla liberada");
            showEvent(e);
        }

    }

}
