/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula02.questao02;

/**
 *
 * @author jorge
 */
public abstract class Coisa implements Comparable<Coisa> {

    @Override
    abstract public int compareTo(Coisa coisa);

}
