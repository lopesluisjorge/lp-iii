/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula04.tratamentodeeventos.exemplo2;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class Painel extends JPanel {

    @Override
    protected void paintComponent(Graphics g) {
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        int[] rgb = {
            (int) (Math.random() * 255),
            (int) (Math.random() * 255),
            (int) (Math.random() * 255)
        };

        Color cor = new Color(rgb[0], rgb[1], rgb[2]);

        g.setColor(cor);
        g.fillOval(70, 70, 100, 100);
    }

}
