/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula04.tratamentodeeventos.exemplo1;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author jorge
 */
public class Tela {

    private JFrame janela;
    private JButton botao;

    public Tela() {
        this.setUp();
    }

    public void setUp() {

        this.janela = new JFrame("Janela");
        this.botao = new JButton("Clique aqui");

        this.botao.addActionListener(new PrintListener());

        this.janela.add(this.botao);

        this.janela.setSize(200, 200);
        this.janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.janela.setVisible(true);
    }

}
