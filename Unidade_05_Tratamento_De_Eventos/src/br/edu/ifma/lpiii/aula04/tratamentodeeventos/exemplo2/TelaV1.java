/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.aula04.tratamentodeeventos.exemplo2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaV1 {

    private JFrame janela;
    private JLabel rotulo;
    private JButton botao;
    private JButton botaoDireita;

    public TelaV1() {
        this.setUp();
    }

    public void setUp() {
        this.janela = new JFrame("Janela Botao Circulo");
        this.janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        this.botao = new JButton("Altera Cor");
        this.botao.addActionListener(new RepintarTela(this.janela));

        this.botaoDireita = new JButton("Botão - Alterar Label");
        this.botaoDireita.addActionListener(new AlterarLabel());

        this.rotulo = new JLabel("Label");
        JPanel painel = new Painel();

        this.janela.add(BorderLayout.SOUTH, this.botao);
        this.janela.add(BorderLayout.CENTER, painel);
        this.janela.add(BorderLayout.EAST, botaoDireita);
        this.janela.add(BorderLayout.WEST, rotulo);

        this.janela.setSize(460, 300);
        this.janela.setVisible(true);
    }

    class RepintarTela implements ActionListener {

        JFrame frame;
        
        public RepintarTela() {
        }

        public RepintarTela(JFrame frame) {
            this.frame = frame;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            frame.repaint();
        }
    }

    class AlterarLabel implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            rotulo.setText("Novo nome no Rótulo");
        }
    }

}
