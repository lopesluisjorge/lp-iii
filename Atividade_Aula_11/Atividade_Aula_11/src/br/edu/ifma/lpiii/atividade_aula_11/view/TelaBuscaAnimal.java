/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.model.Animal;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioAnimais;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaBuscaAnimal extends JFrame {

    private JTextField campoNome;
    private JTextField campoCpf;
    private JTextField campoNomeDono;

    public TelaBuscaAnimal() {
        super("Tela de Busca de Animal");
        this.setUp();
    }

    private void setUp() {
        JPanel painelTitulo = new RotuloTitulo("Buscar Animal");

        JPanel painel = new JPanel();
        GridLayout layoutPanel = new GridLayout(4, 1);
        painel.setLayout(layoutPanel);

        this.campoNome = new JTextField("", 30);
        JButton botao1 = new JButton("Buscar");
        JPanel painel1 = new JPanel();
        painel1.add(campoNome);
        painel1.add(botao1);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Buscar Por Nome do Animal", painel1);

        this.campoCpf = new JTextField("", 20);
        JButton botao2 = new JButton("Buscar");
        JPanel painel2 = new JPanel();
        painel2.add(campoCpf);
        painel2.add(botao2);
        ComponenteRotuloECampo componenteCpf
                = new ComponenteRotuloECampo("Buscar Por CPF do Proprietário", painel2);

        this.campoNomeDono = new JTextField("", 20);
        JButton botao3 = new JButton("Buscar");
        JPanel painel3 = new JPanel();
        painel3.add(campoNomeDono);
        painel3.add(botao3);
        ComponenteRotuloECampo componenteNomeDono
                = new ComponenteRotuloECampo("Buscar Por Nome do Proprietário", painel3);

        painel.add(componenteNome);
        painel.add(componenteCpf);
        painel.add(componenteNomeDono);

        botao1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Animal animal = new RepositorioAnimais()
                            .recuperarPorNome(campoNome.getText());

                    new TelaDetalhesAnimal(animal);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Animal não encontrado");
                }

                dispose();
            }
        });

        botao2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Animal> animais
                            = new RepositorioAnimais().recuperarPorCpfDoProprietario(campoCpf.getText());
                    new TelaListaDeAnimais(animais);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Proprietário sem registros de animais");
                }

                dispose();
            }
        });

        botao3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Animal> animais
                            = new RepositorioAnimais().recuperarPorNomeDoProprietario(campoNomeDono.getText());
                    new TelaListaDeAnimais(animais);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Proprietário sem registros de animais");
                }

                dispose();
            }
        });

        ComponenteVoltarTelaInicial painelVoltar = new ComponenteVoltarTelaInicial(this);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painel);
        this.add(BorderLayout.SOUTH, painelVoltar);

        this.setSize(480, 320);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
