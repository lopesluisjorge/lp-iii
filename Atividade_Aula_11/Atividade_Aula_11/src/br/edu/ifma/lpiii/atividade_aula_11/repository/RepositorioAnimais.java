/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.repository;

import br.edu.ifma.lpiii.atividade_aula_11.exception.AnimalNaoEncontradoException;
import br.edu.ifma.lpiii.atividade_aula_11.model.Animal;
import br.edu.ifma.lpiii.atividade_aula_11.model.Proprietario;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author jorge
 */
public class RepositorioAnimais {

    final private HashMap<Integer, Animal> animais;
    private static Integer ultimoIdInserido = 0;

    public RepositorioAnimais() {
        this.animais = lerArquivo("files/animais.txt", new RepositorioProprietarios());
    }

    public void adicionarAnimal(Animal animal) {
        if (animais.containsKey(animal.getId())) {
            throw new RuntimeException("Animal já existe");
        }

        animal.setId(++ultimoIdInserido);
        this.animais.put(animal.getId(), animal);
        escreverArquivo("files/animais.txt", animal);
    }

    public Animal recuperarPorId(Integer id) {
        if (!this.animais.containsKey(id)) {
            throw new AnimalNaoEncontradoException();
        }

        Animal animal = this.animais.get(id);

        return animal;
    }

    public Animal recuperarPorNome(String nome) {
        return this.animais
                .values()
                .stream()
                .filter((animal) -> {
                    return nome.equals(animal.getNome());
                })
                .iterator()
                .next();
    }

    public ArrayList<Animal> recuperarPorCpfDoProprietario(String cpfDono) {
        ArrayList<Animal> animais = new ArrayList<>();

        Iterator<Animal> iter = this.animais
                .values()
                .stream()
                .filter((animal) -> {
                    return cpfDono.equals(animal.getProprietario().getCpf());
                })
                .iterator();

        if (!iter.hasNext()) {
            throw new RuntimeException();
        }

        while (iter.hasNext()) {
            animais.add(iter.next());
        }

        return animais;
    }

    public ArrayList<Animal> recuperarPorNomeDoProprietario(String nomeDono) {
        Proprietario proprietario
                = new RepositorioProprietarios().recuperarPorNome(nomeDono);

        return this.recuperarPorCpfDoProprietario(proprietario.getCpf());
    }

    private static HashMap<Integer, Animal> lerArquivo(
            String path, RepositorioProprietarios proprietarios) {
        HashMap<Integer, Animal> objetos = new HashMap<>();

        try (Scanner arquivo = new Scanner(new File(path));) {
            while (arquivo.hasNext()) {
                String leitura = arquivo.nextLine();

                StringTokenizer token = new StringTokenizer(leitura, "::");

                Integer id = Integer.parseInt(token.nextToken());
                String cpfProprietario = token.nextToken();
                String nome = token.nextToken();
                String especie = token.nextToken();
                String raca = token.nextToken();
                Integer dia = Integer.parseInt(token.nextToken());
                Integer mes = Integer.parseInt(token.nextToken());
                Integer ano = Integer.parseInt(token.nextToken());

                Proprietario proprietario = proprietarios.recuperarPorCpf(cpfProprietario);

                GregorianCalendar dataDeNascimento = new GregorianCalendar(ano, mes, dia);

                Animal animal = new Animal(
                        proprietario, nome, especie, raca, dataDeNascimento);
                animal.setId(id);
                objetos.put(id, animal);

                ultimoIdInserido = id;
            }
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Repositório de Animais não encontrado");
        }

        return objetos;
    }

    private static void escreverArquivo(String path, Animal obj) {
        try (FileWriter arquivo = new FileWriter(new File(path), true)) {
            try (BufferedWriter buffer = new BufferedWriter(arquivo)) {

                StringBuilder builder = new StringBuilder();

                builder.append(obj.getId()).append("::");
                builder.append(obj.getProprietario().getCpf()).append("::");
                builder.append(obj.getNome()).append("::");
                builder.append(obj.getEspecie()).append("::");
                builder.append(obj.getRaca()).append("::");
                builder.append(obj.getDataDeNascimento()
                        .get(GregorianCalendar.DAY_OF_MONTH)).append("::");
                builder.append(obj.getDataDeNascimento()
                        .get(GregorianCalendar.MONTH)).append("::");
                builder.append(obj.getDataDeNascimento()
                        .get(GregorianCalendar.YEAR));

                buffer.write(builder.toString());
                buffer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException("Erro ao abrir repositório de Animais");
        }
    }

}
