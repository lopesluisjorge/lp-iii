/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.component;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class ComponenteBotoesRodape extends JPanel {

    public ComponenteBotoesRodape(JComponent... components) {
        for (JComponent component : components) {
            this.add(component);
        }
    }

}
