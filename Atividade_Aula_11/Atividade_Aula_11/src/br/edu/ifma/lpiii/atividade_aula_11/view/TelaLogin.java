/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteBotoesRodape;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.controller.ControladorAutenticacao;
import br.edu.ifma.lpiii.atividade_aula_11.controller.ControladorInicial;
import br.edu.ifma.lpiii.atividade_aula_11.exception.UsuarioNaoEncontradoException;
import br.edu.ifma.lpiii.atividade_aula_11.exception.UsuarioOuSenhaInvalidaException;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaLogin extends JFrame {

    private JTextField campoNickname;
    private JPasswordField campoSenha;

    public TelaLogin() {
        super("Login");
        this.setUp();
    }

    private void setUp() {
        //RotuloTitulo titulo = new RotuloTitulo("Login no Sistema");

        JPanel painelForm = new JPanel();
        GridLayout layoutPanel = new GridLayout(4, 1);
        painelForm.setLayout(layoutPanel);

        Icon icone = new ImageIcon("files/pet2.png");

        ComponenteRotuloECampo componenteIcone
                = new ComponenteRotuloECampo("", new JLabel(icone), FlowLayout.CENTER);

        this.campoNickname = new JTextField("", 20);
        ComponenteRotuloECampo componenteNickname
                = new ComponenteRotuloECampo("Usuario", this.campoNickname, FlowLayout.CENTER);

        this.campoSenha = new JPasswordField("", 20);
        ComponenteRotuloECampo componenteSenha
                = new ComponenteRotuloECampo("Senha", this.campoSenha, FlowLayout.CENTER);

        painelForm.add(componenteIcone);
        painelForm.add(componenteNickname);
        painelForm.add(componenteSenha);

        JButton botaoLogin = new JButton("Entrar");
        JButton botaoSair = new JButton("Sair");

        botaoLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nickname = campoNickname.getText();
                String senha = campoSenha.getText();

                try {
                    new ControladorAutenticacao().login(nickname, senha);

                    JOptionPane.showMessageDialog(botaoLogin, "Login efetuado com sucesso!");

                    new ControladorInicial().mostrarTelaInicial();

                    dispose();
                } catch (UsuarioNaoEncontradoException | UsuarioOuSenhaInvalidaException err) {
                    JOptionPane.showMessageDialog(botaoLogin, err);

                    System.err.println(err);
                }
            }
        });

        botaoSair.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        ComponenteBotoesRodape painelBotoes = new ComponenteBotoesRodape(botaoLogin, botaoSair);

        //this.add(BorderLayout.NORTH, titulo);
        this.add(BorderLayout.CENTER, painelForm);
        this.add(BorderLayout.SOUTH, painelBotoes);

        this.setSize(600, 360);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
