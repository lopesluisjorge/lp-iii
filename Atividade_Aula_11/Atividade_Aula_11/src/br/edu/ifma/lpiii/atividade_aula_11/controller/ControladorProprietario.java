/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.controller;

import br.edu.ifma.lpiii.atividade_aula_11.view.TelaBuscaProprietario;
import br.edu.ifma.lpiii.atividade_aula_11.view.TelaCadastroProprietario;

/**
 *
 * @author jorge
 */
public class ControladorProprietario {

    public void mostrarTelaCadastroProprietario() {
        new TelaCadastroProprietario();
    }

    public void mostrarTelaBuscaProprietario() {
        new TelaBuscaProprietario();
    }

}
