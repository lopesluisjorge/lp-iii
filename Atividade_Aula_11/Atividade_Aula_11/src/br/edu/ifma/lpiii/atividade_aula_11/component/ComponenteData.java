/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.component;

import java.util.GregorianCalendar;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class ComponenteData extends JPanel {

    private JTextField campoDia;
    private JComboBox campoMes;
    private JTextField campoAno;

    public ComponenteData() {
        super();

        this.setUp();
    }

    private void setUp() {
        this.campoDia = new JTextField("", 4);
        this.add(this.campoDia);
        this.add(new JLabel("/"));
        this.campoMes = new JComboBox(this.fornecerMeses());
        this.add(this.campoMes);
        this.add(new JLabel("/"));
        this.campoAno = new JTextField("", 6);
        this.add(this.campoAno);
    }

    private String[] fornecerMeses() {
        return new String[]{
            "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"
        };
    }

    public GregorianCalendar parseToGregorianCalendar() {
        int dia = Integer.parseInt(this.campoDia.getText());
        int mes = this.campoMes.getSelectedIndex() + 1;
        int ano = Integer.parseInt(this.campoAno.getText());

        return new GregorianCalendar(ano, mes, dia);
    }

}
