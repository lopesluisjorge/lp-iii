/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.controller;

import br.edu.ifma.lpiii.atividade_aula_11.exception.UsuarioOuSenhaInvalidaException;
import br.edu.ifma.lpiii.atividade_aula_11.model.Usuario;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioUsuarios;
import br.edu.ifma.lpiii.atividade_aula_11.view.TelaLogin;
import br.edu.ifma.lpiii.atividade_aula_11.view.TelaCadastroUsuario;

/**
 *
 * @author jorge
 */
public class ControladorAutenticacao {

    private RepositorioUsuarios usuarios;
    private static Usuario logado;

    public ControladorAutenticacao() {
        this.usuarios = new RepositorioUsuarios();
    }

    public void mostrarTelaLogin() {
        new TelaLogin();
    }

    public void mostrarTelaRegistroUsuario() {
        new TelaCadastroUsuario();
    }

    public void registrar(Usuario usuario) {
        this.usuarios.adicionarUsuario(usuario);
    }

    public void login(String nickname, String senha) {
        Usuario usuario = this.usuarios.recuperarPorNickname(nickname);

        if (!usuario.getSenha().equals(senha)) {
            throw new UsuarioOuSenhaInvalidaException();
        }

        logado = usuario;
    }

    public boolean logout() {
        logado = null;
        return true;
    }

    public boolean isLogado() {
        return logado != null;
    }

    public boolean isLogadoAsAdmin() {
        return logado.isAdmin();
    }

}
