/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.model;

import java.util.GregorianCalendar;

/**
 *
 * @author jorge
 */
public class Animal {

    private Integer id;

    private Proprietario proprietario;

    private String nome;

    private String especie;

    private String raca;

    private GregorianCalendar dataDeNascimento;

    public Animal(Proprietario proprietario, String nome, String especie, String raca, GregorianCalendar dataDeNascimento) {
        this.proprietario = proprietario;
        this.nome = nome;
        this.especie = especie;
        this.raca = raca;
        this.dataDeNascimento = dataDeNascimento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Proprietario getProprietario() {
        return proprietario;
    }

    public void setProprietario(Proprietario proprietario) {
        this.proprietario = proprietario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public GregorianCalendar getDataDeNascimento() {
        return dataDeNascimento;
    }
    
    public String getStringDataDeNascimento() {
        return dataDeNascimento.get(GregorianCalendar.DAY_OF_MONTH) 
                + "/" + dataDeNascimento.get(GregorianCalendar.MONTH) 
                + "/" + dataDeNascimento.get(GregorianCalendar.YEAR);
    }

    public void setDataDeNascimento(GregorianCalendar dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("( ");
        builder.append(nome).append("; ");
        builder.append(especie).append("; ");
        builder.append(raca).append("; ");
        builder.append(dataDeNascimento);
        builder.append(" )");

        return builder.toString();
    }

}
