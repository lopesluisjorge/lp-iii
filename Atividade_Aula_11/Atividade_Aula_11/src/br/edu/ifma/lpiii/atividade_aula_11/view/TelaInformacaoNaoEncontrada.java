/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaInformacaoNaoEncontrada extends JFrame {

    public TelaInformacaoNaoEncontrada(String titulo) {
        super(titulo);
        this.setUp(titulo);
    }

    private void setUp(String titulo) {
        JPanel painel = new JPanel();

        JLabel lblNaoEncontrado = new JLabel(titulo);

        lblNaoEncontrado.setFont(new Font("Tahoma", Font.ITALIC | Font.BOLD, 20));
        lblNaoEncontrado.setForeground(Color.red);

        painel.add(lblNaoEncontrado);

        ComponenteVoltarTelaInicial painelFecharTela = new ComponenteVoltarTelaInicial(this);

        this.add(BorderLayout.CENTER, painel);
        this.add(BorderLayout.SOUTH, painelFecharTela);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }

}
