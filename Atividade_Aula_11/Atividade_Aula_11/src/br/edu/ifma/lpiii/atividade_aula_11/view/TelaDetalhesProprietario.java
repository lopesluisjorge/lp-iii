/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.model.Proprietario;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioProprietarios;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaDetalhesProprietario extends JFrame {

    final private Proprietario proprietario;

    public TelaDetalhesProprietario(Proprietario proprietario) {
        super("Detalhes do Proprietario");
        this.proprietario = proprietario;
        this.setUp();
    }

    private void setUp() {
        RotuloTitulo painelTitulo = new RotuloTitulo("Detalhes do Proprietario");

        JPanel painel = new JPanel();
        GridLayout layoutPanel = new GridLayout(8, 1);
        painel.setLayout(layoutPanel);

        JTextField campoNome = new JTextField(this.proprietario.getNome(), 30);
        campoNome.setEnabled(false);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Nome", campoNome);

        JTextField campoCpf = new JTextField(this.proprietario.getCpf(), 20);
        campoCpf.setEnabled(false);
        ComponenteRotuloECampo componenteCpf
                = new ComponenteRotuloECampo("CPF", campoCpf);

        JTextField campoEndereco = new JTextField(this.proprietario.getEndereco().getEndereco(), 30);
        campoEndereco.setEnabled(false);
        ComponenteRotuloECampo componenteEndereco
                = new ComponenteRotuloECampo("Endereço", campoEndereco);

        JTextField campoCidade = new JTextField(this.proprietario.getEndereco().getCidade(), 20);
        campoCidade.setEnabled(false);
        ComponenteRotuloECampo componenteCidade
                = new ComponenteRotuloECampo("Cidade", campoCidade);

        JTextField campoEstado = new JTextField(this.proprietario.getEndereco().getEstado(), 20);
        campoEstado.setEnabled(false);
        ComponenteRotuloECampo componenteEstado
                = new ComponenteRotuloECampo("Estado", campoEstado);

        JTextField campoNascimento = new JTextField(this.proprietario.getStringDataDeNascimento(), 20);
        campoNascimento.setEnabled(false);
        ComponenteRotuloECampo componenteDataDeNascimento
                = new ComponenteRotuloECampo("Data de Nascimento", campoNascimento);

        JTextField campoTelefone = new JTextField(this.proprietario.getTelefone(), 20);
        campoTelefone.setEnabled(false);
        ComponenteRotuloECampo componenteTelefone
                = new ComponenteRotuloECampo("Telefone", campoTelefone);

        JButton botaoListar = new JButton("Listar Animais");
        JButton botaoAdicionar = new JButton("Adicionar Animal");

        JPanel painel1 = new JPanel();
        painel1.add(botaoListar);
        painel1.add(botaoAdicionar);

        ComponenteRotuloECampo componenteAnimais
                = new ComponenteRotuloECampo("Animais", painel1);

        ComponenteVoltarTelaInicial painelFecharTela
                = new ComponenteVoltarTelaInicial(this);

        botaoListar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    new TelaListaDeAnimais(new RepositorioProprietarios().recuperarAnimaisPorCpf(proprietario.getCpf()));
                } catch (RuntimeException rex) {
                    JOptionPane.showMessageDialog(botaoListar, "Proprietário não possui animais Vinculados");
                }
            }
        });

        botaoAdicionar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaCadastroAnimal();
            }
        });

        painel.add(componenteNome);
        painel.add(componenteCpf);
        painel.add(componenteDataDeNascimento);
        painel.add(componenteEndereco);
        painel.add(componenteCidade);
        painel.add(componenteEstado);
        painel.add(componenteTelefone);
        painel.add(componenteAnimais);

        this.add(painelTitulo, BorderLayout.NORTH);
        this.add(painel, BorderLayout.CENTER);
        this.add(BorderLayout.SOUTH, painelFecharTela);

        this.setSize(480, 520);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
