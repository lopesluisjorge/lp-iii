/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.component;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class ComponenteRotuloECampo extends JPanel {

    public ComponenteRotuloECampo(
            String texto, JComponent componente, int layout) {
        super();
        this.setUp(texto, componente, layout);
    }

    public ComponenteRotuloECampo(
            String texto, JComponent componente) {
        super();
        this.setUp(texto, componente, FlowLayout.LEFT);
    }

    private void setUp(String texto, JComponent componente, int layout) {
        GridBagConstraints gcons = new GridBagConstraints();
        GridBagLayout grid = new GridBagLayout();

        this.setLayout(grid);
        gcons.fill = GridBagConstraints.BOTH;
        gcons.weightx = 1.0;

        gcons.gridwidth = GridBagConstraints.REMAINDER;

        JPanel paniel1 = new JPanel();
        paniel1.setLayout(new FlowLayout(layout));
        paniel1.add(new JLabel(texto));
        this.adicionaComponente(paniel1, grid, gcons);

        gcons.gridwidth = GridBagConstraints.RELATIVE;

        JPanel paniel2 = new JPanel();
        paniel2.setLayout(new FlowLayout(layout));
        paniel2.add(componente);
        this.adicionaComponente(paniel2, grid, gcons);
    }

    private void adicionaComponente(JComponent component, GridBagLayout gb, GridBagConstraints gc) {
        gb.setConstraints(component, gc);
        this.add(component);
    }

}
