/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.model.Animal;
import java.awt.BorderLayout;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author jorge
 */
public class TelaListaDeAnimais extends JFrame {

    private ArrayList<Animal> animais;

    public TelaListaDeAnimais(ArrayList<Animal> animais) {
        super("Animais");

        this.animais = animais;

        String[][] dados = this.formatarLinhas(animais);

        this.setUp(dados);
    }

    private void setUp(String[][] tuplas) {
        Object[] campos = {
            "Id",
            "Nome",
            "Espécie",
            "Raça",
            "Data de Nascimento"
        };

        JTable tabela = new JTable(tuplas, campos);
        JScrollPane scrollPane = new JScrollPane(tabela);

        ComponenteVoltarTelaInicial fecharTela
                = new ComponenteVoltarTelaInicial(this);

        this.add(BorderLayout.CENTER, scrollPane);
        this.add(BorderLayout.SOUTH, fecharTela);

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(480, 360);
        this.setVisible(true);
    }

    private String[][] formatarLinhas(ArrayList<Animal> animais) {
        int tamanho = animais.size();

        String[][] dados = new String[tamanho][5];

        int i = 0;
        for (Animal animal : animais) {
            dados[i][0] = "" + animal.getId();
            dados[i][1] = animal.getNome();
            dados[i][2] = animal.getEspecie();
            dados[i][3] = animal.getRaca();
            dados[i][4] = animal.getStringDataDeNascimento();
            i++;
        }

        return dados;
    }

}
