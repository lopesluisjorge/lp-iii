/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.model.Proprietario;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioProprietarios;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaBuscaProprietario extends JFrame {

    private JTextField campoNome;
    private JTextField campoCidade;

    public TelaBuscaProprietario() {
        super("Tela de Busca de Proprietário");
        this.setUp();
    }

    private void setUp() {
        JPanel painelTitulo = new RotuloTitulo("Buscar Proprietario");

        JPanel painel = new JPanel();
        GridLayout layoutPanel = new GridLayout(5, 1);
        painel.setLayout(layoutPanel);

        this.campoNome = new JTextField("", 30);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Buscar Por Nome", this.campoNome, FlowLayout.CENTER);
        JButton botao1 = new JButton("Buscar");
        ComponenteRotuloECampo btn1 = new ComponenteRotuloECampo("", botao1, FlowLayout.CENTER);

        this.campoCidade = new JTextField("", 20);
        ComponenteRotuloECampo componenteCpf
                = new ComponenteRotuloECampo("Buscar Por Cidade", this.campoCidade, FlowLayout.CENTER);
        JButton botao2 = new JButton("Buscar");
        ComponenteRotuloECampo btn2 = new ComponenteRotuloECampo("", botao2, FlowLayout.CENTER);

        painel.add(componenteNome);
        painel.add(btn1);
        painel.add(componenteCpf);
        painel.add(btn2);

        botao1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Proprietario proprietario = new RepositorioProprietarios()
                            .recuperarPorNome(campoNome.getText());

                    new TelaDetalhesProprietario(proprietario);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Proprietario não encontrado");
                }

                dispose();
            }
        });

        botao2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println(campoCidade.getText());
                    System.out.println(new RepositorioProprietarios().recuperarPorCidade(campoCidade.getText()));

                    ArrayList<Proprietario> proprietarios
                            = new RepositorioProprietarios().recuperarPorCidade(campoCidade.getText());

                    new TelaListaDeProprietarios(proprietarios);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Cidade não possui Proprietários");
                }

                dispose();
            }
        });

        ComponenteVoltarTelaInicial painelVoltar = new ComponenteVoltarTelaInicial(this);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painel);
        this.add(BorderLayout.SOUTH, painelVoltar);

        this.setSize(480, 360);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
