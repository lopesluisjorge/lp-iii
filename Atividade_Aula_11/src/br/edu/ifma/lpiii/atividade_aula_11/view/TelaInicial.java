/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.controller.ControladorAutenticacao;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author jorge
 */
public class TelaInicial extends JFrame {

    public TelaInicial() {
        super("Tela Inicial");
        this.setUp();
    }

    private void setUp() {
        RotuloTitulo painelTitulo = new RotuloTitulo("Sistema de PetShop");

        JPanel painelTela = new JPanel();

        painelTela.setLayout(new GridLayout(4, 1));

        JButton btnCadastroProprietario = new JButton("Cadastro de Proprietário");
        JButton btnConsultaProprietario = new JButton("Consulta de Proprietário");
        JButton btnCadastroAnimal = new JButton("Cadastro de Animal");
        JButton btnConsultaAnimal = new JButton("Consulta de Animal");
        JButton btnCadastroUsuario = new JButton("Cadastro de Usuário");
        JButton btnConsultaUsuario = new JButton("Consulta de Usuário");

        btnCadastroProprietario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaCadastroProprietario();
            }
        });

        btnConsultaProprietario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaBuscaProprietario();
            }
        });

        btnCadastroAnimal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaCadastroAnimal();
            }
        });

        btnConsultaAnimal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaBuscaAnimal();
            }
        });

        btnCadastroUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaCadastroUsuario();
            }
        });

        btnConsultaUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaBuscaUsuario();
            }
        });

        JPanel panelIcone = new JPanel();
        Icon icone = new ImageIcon("files/pet2.png");
        panelIcone.add(new JLabel(icone));

        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);

        JPanel painelProprietario = new JPanel(layout);
        painelProprietario.add(btnCadastroProprietario);
        painelProprietario.add(btnConsultaProprietario);

        JPanel painelAnimal = new JPanel(layout);
        painelAnimal.add(btnCadastroAnimal);
        painelAnimal.add(btnConsultaAnimal);

        JPanel painelUsuario = new JPanel(layout);
        if (new ControladorAutenticacao().isLogadoAsAdmin()) {
            painelUsuario.add(btnCadastroUsuario);
        }
        painelUsuario.add(btnConsultaUsuario);

        painelTela.add(panelIcone);
        painelTela.add(painelProprietario);
        painelTela.add(painelAnimal);
        painelTela.add(painelUsuario);

        JPanel painelSair = new JPanel();

        JButton botaoSair = new JButton("Sair");
        painelSair.add(botaoSair);

        botaoSair.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ControladorAutenticacao().logout();

                new TelaLogin();
                dispose();
            }
        });

        painelSair.add(botaoSair);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painelTela);
        this.add(BorderLayout.SOUTH, painelSair);

        this.setSize(520, 420);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

}
