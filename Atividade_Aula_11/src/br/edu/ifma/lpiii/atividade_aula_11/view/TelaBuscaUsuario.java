/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.model.Usuario;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioUsuarios;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaBuscaUsuario extends JFrame {

    private JTextField campoNickname;

    public TelaBuscaUsuario() {
        super("Tela de Busca de Usuário");
        this.setUp();
    }

    private void setUp() {
        JPanel painelTitulo = new RotuloTitulo("Buscar Usuário");

        JPanel painel = new JPanel();
        GridLayout layoutPanel = new GridLayout(3, 1);
        painel.setLayout(layoutPanel);

        this.campoNickname = new JTextField("", 20);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Buscar Por Nome de Usuário", this.campoNickname, FlowLayout.CENTER);
        JButton botao1 = new JButton("Buscar");
        ComponenteRotuloECampo btn1 = new ComponenteRotuloECampo("", botao1, FlowLayout.CENTER);

        JButton botao2 = new JButton("Buscar");
        ComponenteRotuloECampo btn2 = new ComponenteRotuloECampo("Recuperar Todos os Usuários", botao2, FlowLayout.CENTER);

        painel.add(componenteNome);
        painel.add(btn1);
        painel.add(btn2);

        botao1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Usuario usuario 
                            = new RepositorioUsuarios().recuperarPorNickname(campoNickname.getText());

                    new TelaDetalhesUsuario(usuario);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Usuário não encontrado");
                }

                dispose();
            }
        });

        botao2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ArrayList<Usuario> usuarios
                            = new RepositorioUsuarios().recuperarTodosUsuarios();
                    
                    System.out.println(new RepositorioUsuarios().recuperarTodosUsuarios());
                    new TelaListaDeUsuarios(usuarios);
                } catch (RuntimeException err) {
                    new TelaInformacaoNaoEncontrada("Sem registros de usuários");
                }
                
                dispose();
            }
        });
        
        ComponenteVoltarTelaInicial painelVoltar = new ComponenteVoltarTelaInicial(this);

        this.add(BorderLayout.NORTH, painelTitulo);
        this.add(BorderLayout.CENTER, painel);
        this.add(BorderLayout.SOUTH, painelVoltar);

        this.setSize(480, 240);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

}
