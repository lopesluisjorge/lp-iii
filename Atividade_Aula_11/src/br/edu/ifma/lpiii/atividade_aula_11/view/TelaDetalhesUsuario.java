/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.model.Usuario;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.GregorianCalendar;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaDetalhesUsuario extends JFrame {

    final private Usuario usuario;

    public TelaDetalhesUsuario(Usuario usuario) {
        super("Detalhes do Usuário");
        this.usuario = usuario;
        this.setUp();
    }

    private void setUp() {
        RotuloTitulo painelTitulo = new RotuloTitulo("Detalhes do Usuário");

        JPanel painel = new JPanel();
        GridLayout layoutPanel = new GridLayout(7, 1);
        painel.setLayout(layoutPanel);

        JTextField campoId = new JTextField("" + this.usuario.getId(), 5);
        campoId.setEnabled(false);
        ComponenteRotuloECampo componenteId
                = new ComponenteRotuloECampo("Id", campoId);

        JCheckBox campoAdmin = new JCheckBox();
        campoAdmin.setEnabled(false);
        ComponenteRotuloECampo componenteAdmin
                = new ComponenteRotuloECampo("É administrador", campoAdmin);

        JTextField campoNome = new JTextField(this.usuario.getNome(), 30);
        campoNome.setEnabled(false);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Nome", campoNome);

        JTextField campoNickname = new JTextField(this.usuario.getNickname(), 20);
        campoNickname.setEnabled(false);
        ComponenteRotuloECampo componenteBickname
                = new ComponenteRotuloECampo("Nome de Usuário", campoNickname);

        JTextField campoSenha = new JPasswordField(this.usuario.getSenha(), 20);
        campoSenha.setEnabled(false);
        ComponenteRotuloECampo componenteSenha
                = new ComponenteRotuloECampo("Senha", campoSenha);

        ComponenteVoltarTelaInicial painelFecharTela
                = new ComponenteVoltarTelaInicial(this);

        painel.add(componenteId);
        painel.add(componenteAdmin);
        painel.add(componenteNome);
        painel.add(componenteBickname);
        //painel.add(componenteSenha);

        this.add(painelTitulo, BorderLayout.NORTH);
        this.add(painel, BorderLayout.CENTER);
        this.add(BorderLayout.SOUTH, painelFecharTela);

        this.setSize(480, 480);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    public String calculaIdade(GregorianCalendar gc) {
        GregorianCalendar agora = new GregorianCalendar();

        int anos;
        if (gc.get(GregorianCalendar.DAY_OF_YEAR) > agora.get(GregorianCalendar.DAY_OF_YEAR)) {
            anos = agora.get(GregorianCalendar.YEAR) - gc.get(GregorianCalendar.YEAR);
        } else {
            anos = agora.get(GregorianCalendar.YEAR) - gc.get(GregorianCalendar.YEAR) - 1;
            if (anos < 0) {
                anos = 0;
            }
        }

        int meses = (agora.get(GregorianCalendar.MONTH) - gc.get(GregorianCalendar.MONTH)) % 12;

        return anos + " anos e " + meses + " meses";
    }

}
