/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.view;

import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteRotuloECampo;
import br.edu.ifma.lpiii.atividade_aula_11.component.ComponenteVoltarTelaInicial;
import br.edu.ifma.lpiii.atividade_aula_11.model.Animal;
import br.edu.ifma.lpiii.atividade_aula_11.component.RotuloTitulo;
import br.edu.ifma.lpiii.atividade_aula_11.repository.RepositorioProprietarios;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class TelaDetalhesAnimal extends JFrame {

    final private Animal animal;

    public TelaDetalhesAnimal(Animal animal) {
        super("Detalhes do Animal");
        this.animal = animal;
        this.setUp();
    }

    private void setUp() {
        RotuloTitulo painelTitulo = new RotuloTitulo("Detalhes do Animal");

        JPanel painel = new JPanel();
        GridLayout layoutPanel = new GridLayout(7, 1);
        painel.setLayout(layoutPanel);

        JTextField campoId = new JTextField("" + this.animal.getId(), 5);
        campoId.setEnabled(false);
        ComponenteRotuloECampo componenteId
                = new ComponenteRotuloECampo("Id", campoId);

        JTextField campoNome = new JTextField(this.animal.getNome(), 30);
        campoNome.setEnabled(false);
        ComponenteRotuloECampo componenteNome
                = new ComponenteRotuloECampo("Nome", campoNome);

        JTextField campoEspecie = new JTextField(this.animal.getEspecie(), 20);
        campoEspecie.setEnabled(false);
        ComponenteRotuloECampo componenteEspecie
                = new ComponenteRotuloECampo("Espécie", campoEspecie);

        JTextField campoRaca = new JTextField(this.animal.getRaca(), 20);
        campoRaca.setEnabled(false);
        ComponenteRotuloECampo componenteRaca
                = new ComponenteRotuloECampo("Raça", campoRaca);

        JTextField campoNascimento = new JTextField(this.animal.getStringDataDeNascimento(), 20);
        campoNascimento.setEnabled(false);
        ComponenteRotuloECampo componenteDataDeNascimento
                = new ComponenteRotuloECampo("Data de Nascimento", campoNascimento);

        JTextField campoIdade = new JTextField(this.calculaIdade(this.animal.getDataDeNascimento()), 20);
        campoIdade.setEnabled(false);
        ComponenteRotuloECampo componenteIdade
                = new ComponenteRotuloECampo("Idade", campoIdade);

        JButton botaoProprietario = new JButton("Visualizar");
        ComponenteRotuloECampo componenteProprietario
                = new ComponenteRotuloECampo("Proprietario", botaoProprietario);

        ComponenteVoltarTelaInicial painelFecharTela
                = new ComponenteVoltarTelaInicial(this);

        botaoProprietario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TelaDetalhesProprietario(new RepositorioProprietarios().recuperarPorCpf(animal.getProprietario().getCpf()));
            }
        });

        painel.add(componenteId);
        painel.add(componenteNome);
        painel.add(componenteEspecie);
        painel.add(componenteRaca);
        painel.add(componenteDataDeNascimento);
        painel.add(componenteIdade);
        painel.add(componenteProprietario);

        this.add(painelTitulo, BorderLayout.NORTH);
        this.add(painel, BorderLayout.CENTER);
        this.add(BorderLayout.SOUTH, painelFecharTela);

        this.setSize(480, 480);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
    }

    public String calculaIdade(GregorianCalendar gc) {
        GregorianCalendar agora = new GregorianCalendar();

        int anos;
        if (gc.get(GregorianCalendar.DAY_OF_YEAR) > agora.get(GregorianCalendar.DAY_OF_YEAR)) {
            anos = agora.get(GregorianCalendar.YEAR) - gc.get(GregorianCalendar.YEAR);
        } else {
            anos = agora.get(GregorianCalendar.YEAR) - gc.get(GregorianCalendar.YEAR) - 1;
            if (anos < 0) {
                anos = 0;
            }
        }

        int meses = (agora.get(GregorianCalendar.MONTH) - gc.get(GregorianCalendar.MONTH)) % 12;

        return anos + " anos e " + meses + " meses";
    }

}
