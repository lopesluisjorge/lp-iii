/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.controller;

import br.edu.ifma.lpiii.atividade_aula_11.view.TelaBuscaAnimal;
import br.edu.ifma.lpiii.atividade_aula_11.view.TelaCadastroAnimal;

/**
 *
 * @author jorge
 */
public class ControladorAnimal {

    public void mostrarTelaCadastroAnimal() {
        new TelaCadastroAnimal();
    }

    public void mostrarTelaConsultaAnimal() {
        new TelaBuscaAnimal();
    }

}
