/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifma.lpiii.atividade_aula_11.component;

import br.edu.ifma.lpiii.atividade_aula_11.model.Endereco;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author jorge
 */
public class ComponenteEndereco extends JPanel {

    private JTextField campoRua;
    private JTextField campoCasa;
    private JTextField campoBairro;
    private JTextField campoCidade;
    private ComponenteEstados campoEstados;

    public ComponenteEndereco() {
        super();
        this.setUp();
    }

    private void setUp() {
        GridBagConstraints gcons = new GridBagConstraints();
        GridBagLayout layout1 = new GridBagLayout();

        this.setLayout(layout1);

        gcons.fill = GridBagConstraints.BOTH;
        gcons.weightx = 1.0;

        this.campoRua = new JTextField("", 25);
        ComponenteRotuloECampo componenteRua
                = new ComponenteRotuloECampo("Logradouro", this.campoRua);
        this.adicionaComponente(componenteRua, layout1, gcons);

        this.campoCasa = new JTextField("", 5);
        ComponenteRotuloECampo componenteCasa
                = new ComponenteRotuloECampo("Número", this.campoCasa);
        this.adicionaComponente(componenteCasa, layout1, gcons);

        this.campoBairro = new JTextField("", 20);
        ComponenteRotuloECampo componenteBairro
                = new ComponenteRotuloECampo("Bairro", this.campoBairro);
        this.adicionaComponente(componenteBairro, layout1, gcons);

        this.campoCidade = new JTextField("", 20);
        ComponenteRotuloECampo componenteCidade
                = new ComponenteRotuloECampo("Cidade", this.campoCidade);
        this.adicionaComponente(componenteCidade, layout1, gcons);

        gcons.gridwidth = GridBagConstraints.REMAINDER;

        this.campoEstados = new ComponenteEstados();
        ComponenteRotuloECampo componenteEstados
                = new ComponenteRotuloECampo("Estado", this.campoEstados);
        this.adicionaComponente(componenteEstados, layout1, gcons);

    }

    private void adicionaComponente(JComponent component, GridBagLayout gb, GridBagConstraints gc) {
        gb.setConstraints(component, gc);
        this.add(component);
    }

    public Endereco recuperarEndereco() {
        StringBuilder endereco = new StringBuilder();

        endereco.append(this.campoRua.getText()).append(", ");
        endereco.append(this.campoCasa.getText()).append(", ");
        endereco.append(this.campoBairro.getText());
        String cidade = this.campoCidade.getText();
        String estado = this.campoEstados.getNomeEstado();

        return new Endereco(endereco.toString(), cidade, estado);
    }

}
